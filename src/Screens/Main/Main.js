import React from 'react'
import Card from '../../components/Card/card'
import axios from 'axios'

class Main extends React.Component {
  constructor(...props) {
    super(props)
    this.state =
    {
      personajes: [],
      search: ''
    }
  }
  componentDidMount() {
    axios.get('https://swapi.co/api/people/1/')
      .then(function (response) 
      {
        const {name, gender, height, hair_color:hairColor} = response.data
        let personaje = [...this.state.personajes, {
                          name,
                          gender,
                          height,
                          hairColor
                        }]
        
          this.setState = {
            personaje
          }
        }
      )
      .catch(function (error) 
      {

        // handle error
        console.log(error);
      })
      .finally(function () 
      {

        // always executed
      });
  }
  render() {
    return <form>
      <h1 class="text-primary display-4">{this.props.text}</h1>

      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text text-black">Introduce los ids separados por comas:</span>
        </div>
        <input type="text" onChange={this.onChangeSearch} aria-label="First name" class="form-control"></input>
      </div>

      <button class="btn btn-block btn-success">Submit</button>
      <div className="col-md-4">
        {this.state.personajes.map((personaje, index) => {
          return <Card key={index} personaje={personaje}></Card>
          //return <h1>{this.state.test}</h1>
        })}
      </div>

    </form>
  }
}
export default Main;