import React from 'react'

class Input extends React.Component {
  render() {
    return <form>
      <h1 class="text-primary display-4">{this.props.text}</h1>

      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text text-black">Introduce los ids separados por comas:</span>
        </div>
        <input type="text" aria-label="First name" class="form-control"></input>
      </div>
      <button class="btn btn-block btn-success">Submit</button>
    </form>
  }
}
export default Input;