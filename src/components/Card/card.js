import React from 'react'

class Card extends React.Component 
{
    render() 
    {
        return <form>
            <div>
                <header>
                </header>
                <section>
                    <div class="list-group">
                        <button type="button" class="list-group-item list-group-item-action">Nombre del personaje: {this.props.personaje.name}</button>
                        <button type="button" class="list-group-item list-group-item-action">Género: {this.props.personaje.gender}</button>
                        <button type="button" class="list-group-item list-group-item-action">Estatura: {this.props.personaje.height}</button>
                        <button type="button" class="list-group-item list-group-item-action">Color de cabello: {this.props.personaje.hairColor}</button>
                    </div>
                </section>
            </div>
        </form>
    }
}
export default Card;