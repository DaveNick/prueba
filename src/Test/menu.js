import React from 'react'

class Menu extends React.Component 
{
    render() 
    {
        return <div class="navbar navbar-expand-sm bg-dark">
            <ul class="navbar-nav">
                <a href="#" class="navbar-brand">LOGO</a>
                <li class="nav-item"><a href="#" class="nav-link text-danger">Link 1</a></li>
                <li class="nav-item"><a href="#" class="nav-link text-danger">Link 2</a></li>
                <li class="nav-item"><a href="#" class="nav-link text-danger">Link 3</a></li>
                <li class="nav-item"><a href="#" class="nav-link text-danger">Link 4</a></li>
            </ul>
        </div>
    }
}
export default Menu;