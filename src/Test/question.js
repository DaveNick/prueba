import React from 'react'

class Question extends React.Component {
    render() {
        return <div id="acordion">
        <div class="card">
          <div class="card-header">
            <a href="#uno" class="card-link text-info" data-toggle="collapse" data-parent="#acordion">Pregunta 1</a>
          </div>
          <div class="collapse" id="uno">
            <div class="card-body">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad a iste cum. Nam necessitatibus nostrum numquam, modi quod, nobis non, distinctio ad suscipit minima sit minus totam similique nihil facilis.
            </div>
          </div>
          <div class="card-header">
            <a href="#dos" class="card-link text-info" data-toggle="collapse" data-parent="#acordion">Pregunta 2</a>
          </div>
          <div class="collapse" id="dos">
            <div class="card-body">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad a iste cum. Nam necessitatibus nostrum numquam, modi quod, nobis non, distinctio ad suscipit minima sit minus totam similique nihil facilis.
            </div>
          </div>
          <div class="card-header">
            <a href="#tres" class="card-link text-info" data-toggle="collapse" data-parent="#acordion">Pregunta 3</a>
          </div>
          <div class="collapse" id="tres">
            <div class="card-body">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad a iste cum. Nam necessitatibus nostrum numquam, modi quod, nobis non, distinctio ad suscipit minima sit minus totam similique nihil facilis.
            </div>
          </div>
          <div class="card-header">
            <a href="#cuatro" class="card-link text-info" data-toggle="collapse" data-parent="#acordion">Pregunta 4</a>
          </div>
          <div class="collapse" id="cuatro">
            <div class="card-body">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad a iste cum. Nam necessitatibus nostrum numquam, modi quod, nobis non, distinctio ad suscipit minima sit minus totam similique nihil facilis.
            </div>
          </div>
          <div class="card-header">
            <a href="#cinco" class="card-link text-info" data-toggle="collapse" data-parent="#acordion">Pregunta 5</a>
          </div>
          <div class="collapse" id="cinco">
            <div class="card-body">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad a iste cum. Nam necessitatibus nostrum numquam, modi quod, nobis non, distinctio ad suscipit minima sit minus totam similique nihil facilis.
            </div>
          </div>
        </div>
      </div>
                }
              }
export default Question;