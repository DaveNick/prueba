import React from 'react'

class Form extends React.Component 
{
    render() 
    {
        return <form action="archivo.php" method="POST" class="form-inline">
        <label for="nombre">Nombre:</label>
            <input type="text"      class="form-control" id="nombre"></input>
        <label for="apellido">Apellido:</label>
            <input type="text"      class="form-control" id="apellido"></input>
        <label for="edad">Edad:</label>
            <input type="number"    class="form-control" id="edad"></input>
        <label for="contraseña">Contraseña:</label>
            <input type="password"  class="form-control" id="contraseña"></input>
            <input type="submit" value="ENVIAR" class="btn btn-info"></input>
    </form>
    }
}
export default Form;